# fis-industria

___Mapas conceptuales___   (v2)

[//]: # (!PlantUML model)

## Industria 4.0 :department_store:


#### Sintaxis Markdown
```plantuml
@startmindmap
*[#FFBBCC] Industria 4.0
	* Historia previa a la Industria 4.0
		*[#lightgreen] **Industria 1.0** (1784)
			*[#lightblue] Se caracterizó por: \n*Producción mecánica. \n*Equipos basados en energía a vapor e hidráulica.
		*[#lightgreen] **Industria 2.0** (1870)
			*[#lightblue] Se caracterizó por: \n*La aparición de lineas de ensamblaje. \n*Equipos basados en energía electrica. \n*Producción en masa.
		*[#lightgreen] **Industria 3.0** (1969)
			*[#lightblue] Se caracterizó por: \n*Aparece la producción automatizada usando electronica y TI. \n*Se da lugar a *Programamble logic controller (PLC)*
	* ¿En que consiste la industria 4.0?
		*[#lightgreen] Consiste en una producción inteligente con decisión autónoma. Es decir, la digitalización de los \nprocesos industriales por medio de la interacción de la inteligencia artificial con las máquinas \ny la optimización de recursos enfocada en la creación de efectivas metodologías comerciales.
			*[#lightblue] Las tecnologías involucradas son: \n*loT, IA, Big Data, Cloud, Robótica, entre otras.
	* ¿Que cambios realizó la industria 4.0?
		*[#lightgreen] Integración de TIC's
			*[#lightblue] Esto da como resultado: \n*Procesos cada vez más automatizados. \n*Menos personal humano involucrado.
				*[#FFBBCC] Ejemplos de procesos afectados por la automatización: \n*Producción de bienes. \n*Manejo de inventario. \n*Delivery o entrega de productos.
				*[#FFBBCC] Efectos de la integración de las TIC's:
					*[#Orange] Efecto negativo: Reducción de los puestos de trabajo. Esto se dará gracias a la automatización, \npuesto que la intervención humana será casi nula, en especial en trabajos muy repetitivos.
					*[#Orange] Efecto positivo: Aparición de nuevas profesiones, algunos ejemplos son: \n* Youtubers, consultores de redes sociales, pilotos de drones. 
		*[#lightgreen] Transición de Empresas de manufactura a empresas de TIC’s
			*[#lightblue] Esto da como resultado: \n*La implementación de nuevas tecnologías como un sistema operativo en \nproductos de manufactura más simple, por ejemplo, en aspiradoras, lavadoras, \nrefrigeradores incluso hasta macetas o automóviles. 
				*[#FFBBCC] Efectos: \n*Se desdibuja la diferencia entre las industrias, lo que genera una mayor competencia.
		*[#lightgreen] Aparición de nuevos paradigmas y tecnologías
			*[#lightblue] Esto da como resultado: \n*Se da la aparición de nuevos paradigmas.
				*[#FFBBCC] Paradigma, velocidad en los negocios: Este paradigma consiste en la rápida adaptación a los cambios \ntecnológicos y resulta ser más que necesaria para la subsistencia de una compañía. Los ejemplos más notorios de una falta de adaptación son empresas como Nokia o Kodac.
				*[#FFBBCC] Negocios basados en plataformas FANG y BAT.
		*[#lightgreen] Evolución y aparición de nuevas culturas digitales. 
			*[#lightblue] Esto da como resultado: \n*Una normalización del uso de las tecnologías en la vida cotidiana. \n*Dependencia a la tecnología.

@endmindmap
```
[//]: # (Arithmetic notation: _Click aquí_  :point_right:  [PlantUML 11] :zap:)




## México y la Industria 4.0 :chart_with_upwards_trend:


#### Sintaxis Markdown
```plantuml
@startmindmap
*[#FFBBCC] México y la Industria 4.0
	* Desarrollos tecnológicos:
		*[#lightgreen] El internet de las cosas:  El IoT se refiere a la comunicación Máquina a Máquina (M2M), \nes decir, la conexión entre dispositivos u objetos como automóviles o electrodomésticos; \nla comunicación Máquina a Persona (M2P), y la comunicación Persona a Persona (P2P).
		*[#lightgreen] Startups México: Según su propia página, Startups es la organización líder en México en \nla promoción de la innovación, la cultura emprendedora, de esta se derivan varios \nproyectos, entre ellos:
			*[#FFBBCC] Sin Llave es una startup creada en México que logró ver una oportunidad muy interesante \ncon Airbnb, ya que se trata de una aplicación que permite acceder al inmueble si \nla necesidad de contar con una llave física, solamente con nuestro Smartphone.
			*[#FFBBCC] Wearobot : Es otro ejemplo de los avances que se están dando en México, el proyecto consiste \nen un exoesqueleto permite al usuario realizar su rehabilitación desde casa, ya que se conecta \na una aplicación de Internet que le permite al médico enviar instrucciones de las rutinas y \nmonitorear el avance del paciente. Asimismo, la tecnología avisa a médicos y familiares en caso de algún tipo de accidente.
	* Requerimientos para un avance más notorio: 
		*[#lightgreen] *Mejora en las instalaciones de telecomunicaciones, es decir una conexión a internet más rápida y estable.
		*[#lightgreen] *Una mayor inversión por parte del gobierno.
		*[#lightgreen] *Promover ideas innovadoras.  
	* Adversidades: 
		*[#lightgreen] *La mala infraestructura a nivel nacional dificulta el avance en la llamada 4ta revolución industrial.
		*[#lightgreen] *Letargo significativo en IoT derivado de la mala infraestructura.
		*[#lightgreen] *El casi nulo apoyo hacia las nuevas tecnologías como el IoT o internet de las cosas.
@endmindmap
```
[//]: # (Arithmetic notation: _Click aquí_  :point_right: [PlantUML 2] :zap:)

[PlantUML 1]: http://www.plantuml.com/plantuml/uml/bLRDZbj54ztFKvJqBRvaOba9HEHdGsOS1qOC8OHXWI2BSdVvJWzzkswwkYqvArw3PHw01Pe7GC9lmfFGrVTU-O972RCQ9rq_vvmwLVOpIHXJwu9jiJkwz-F_NhmuFLqiti1Pi5bIT0WFv_UFxjs3BvqardztaJOE0S7ZGLGfu5rpcPf85Dx0yV4-uC7y_l4nt7dmwF73knevXgvyfZVm7O71Y2PHTEzsrz1nV0e_XUDNaMqsnksk0xHaTk-3CpZNb-NRx3eMMA6WBNyf00MApUutnRR1KW48BfsDk_VPQzQ7yNqou7lywFujy9qZO8VHzGWjWNU1KFGJ1S5svV6AFegNF9aKHsPJpbHeetmC-wS3zYUVFRa5zfC2d0pf7Bj9Iyo9Mqpk7Lg4B1Wi3m1PnOSjN9nLdALkYV2vmGW82hk9s2fd0i-DCs0ub2plASATL-UBkyT7wgG__rW6U9j9dyL9gWZSr4cV7z9TZB4K88T_8dOXKOcba6fDi6ISZ5pA7svn3akf_nzdsiswnYNqXUD-S2o5L2bhI5ZsU3o9AWOjMSSr46l3Y6QVYtiCHdSZ9hTsfYHNF1u5sjtxOe20scIh2Tmbrqu1H38vIdN6cesAJveA9j8-YDPaajjeGKfisRCQIC1mIz5KjFD31voZG28Jnd1mOSC-cuWM1OH3zOFdYncSdSpWr3Nm71FEOE4vsncyvjNkEZc3Cw2G8W6dY38VflcjZhACtR509FHgilyoqhEGg8bxRXTdY__B8VYb90QBIf4XacIVqF9u7VffLRasz4vbbgc3MQfLlwR00Xr5uO0UBdEBWISgS2_QvFujhwZjFDTDxiOsgEfhqUe8ZvDEbSN1oIh9AqU11XGOw4ezf9qf98oEwyDpycv3SGlSwzjWRLiB9PP3S8g3PMAzgOuUfTVoQStJn6yYXeR6H0ZKO75H4V4rJR5wPPP9XjefuWglUDwFG7JTop65sWi5y0O5Phf3jKBTy75N2jlwt2kF8AI538g3a7sr5P1qL5U6mjXPeCrRYDHHSeftLEAGKSVYUaOdqtDSgjC6HQLSapWE93D0tsIr0uqZ7XS0Vk2SyefY2J8Sr6aSIVhbjAI12b1hTCxpe9ADN0TyTEZmYuX1zc2MRHT9i6Qr6F8QJSeH0O7sBpgwltxvzNQBS8xW2XTgrLN_9Z_T_3uv1wZ7jDKpHB5w0IYKIgFd3Z3MnPBQPARk7zMheqFfd6vHHD7pkXa-bdgHrj4rL1z9xuxnMHWkKLAfYOOI2d3lejtrnlcgvCrsVpf-wP1OjyfNs5_oDSLwTOVJv75ol1LExyI60aM47BJjbgCAqL6gcVF3qTrW8zqDZ6XTqwB0TYhhxOQb52fs_62FmxL_DJxDO4EUZRDeWK9Tsq0D6rT4qiwqhxB_ajL0XB9tNSa3jDYDLa6jC9xmWh1djhjkdE72T8IlMrjzK0KDP4WmEgozjBJaLUrKXs59jQugu-xtej4SpgSxL-i4JXmTonYzHfzgwXITSDZlHvNp9V_i41Y-OelcGAMNWnBJNrgTnuHhZZgv5oSlloYaJaykRfhxidubrCRR6mQa4BBNZP3nHuGwzbRp_puW1CKo_VtX8GkFrtjghM5k6rUB9MST1Xo-DJeAjfTR1p1DAzIUbSVXX_tV

[PlantUML 21]: http://www.plantuml.com/plantuml/uml/VLKnRoD73-tzAmXypPTO48BWAgSvtyK763W3HfmWHN85jKFBD6Q7U-JCmig_SNc5AtTfzu-5Cwk5X5X8jRQ6V8_laSDvRnarzvn2ZyFP-H__-_JfmuUF7x_0pVJjYJk17KI4wnIAPMM4T-iVpixFuMSoL9KOnI1JboHEhriFjmi_TPJ8suUyLQBq1QuYSCgaYJ84SZW33qIxW7fqBR-24IZTCoa1-haVzoLnXnrFhyahUVvQE27WuS__t_nuyzqA_anaZjcnhkQyH4yjXr9siC0sY77cKGmAoEQHibY55y2IfPzUHuuE8K2HkgmI_BTlbgkIdnpzlsgv9JMPIxdrKdPl2byYZcDlFNPz1czDkgjjA8Cjlb_07MsdlnDOWK5bO8HXUjuwzUeGIrP9HRUO-AyzQPnU0YbGMf20qgp4ONffGNCRW5EISKwRxIin5qMWVb1AWOAeitaiMKOm0WVc4HECg2pcg0wvSz_4Ldl7ASRz31oDqXqd-1nnh41GaaFD0g1Jme37jNuj156sEht2IDg2PH0FJXmmG5zsyooHeN-y0cyvNB9kqiPRW2tV2B9YHWZK0728IqlQ-K3QioTZrr6r2gDZzeKsaS1uzYfHHzOe7SGvlE3wQNYnIRcVNinXLs0IiNVrr09I8SigSDUxnE514gt_vSRlX2eRoN01LmPIW-dHBO_IsY86w1ux_IB60QVd105Ja2EltF88ImCgjx4rIwYM2VGaP8uGASj1SeHYfNOFb33wq6WTCAK7t73aZCjuM9MDXiUcEeLJeJFW6ryz_7gvuws7TCpfTohCapYoCt9oYqhNiGFQiXAqP4x-tJb-BucpA7ainRqT4AfSfwjMh-7Ik6VhfGtkl85aExsWHxCXuC9P0U-nv-Z4JaQfocfEOsptA_Cm_-lrX8PzLXVSB-GobFjA9-R4MaNlQt7mPuCaNYFBnOdR_FqDFOgsoOveJJ16F0ZE5EcmA-glArWscTCSRp8yBC_6gjFpm05T5Zcep-pw1Frl3j7ZJghN8warf46qwiYpbLlPiCFAgVJRkYay3pWGsdvFr6rWQu1cpsLei043sKa3FjS2sewvLwJMxxPR41AFJf-QRemG-9xhui53gvjjxPxrw4hV-PdIAB7CiyRBOnJNfuWfesu5ZBUfGcFT_exO7ffvWONvffsky1JcLQnJmv1A5C11TW8FNZ-svgP28zhn3EvV68gDKqw_VZvcxoc5_Slx3m00

[GarciaMayoralJoseLuis  @ GitLab](https://gitlab.com/GarciaMayoralJoseLuis)
